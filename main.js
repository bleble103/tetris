const mapWidth = 10;
const mapHeight = 20;
const blockSize = 20;
const blocksNo = 7;
const auxCanvasWidth = 6*blockSize;
const backgroundColor = "black";
const blockColor = "lime";
const currentBlockColor = "red";
const endGameBlockColor = "magenta";
const lolBackgroundColor = "white";
const lol2BackgroundColor = "yellow";
const nextBlockColor = "khaki";
const lineColor = "orangered";
const scoreColor = "deepskyblue";
const scoreBackgroundColor = "#666";
const MODE_FREE_BLOCK = 0;
const MODE_CURRENT_BLOCK = 1;
const MODE_NEXT_BLOCK = 2;
const comboMultiplier = 2;
const multipleLineBonus = 20;
var gTime = 500;

var block = false;
var lock = false;
var endGame = false;
var lolCounter = 0;
var score = 0;
var combo = false;

function printDigit(digit, position){
	const sx = 20;
	const sy = 10;
	const digitWidth = 3;
	const digitSpace = 1;

	if(digit < 0 || digit > 9) {console.log("Not a digit!");return;}

	for(var i=0; i<digits[digit].length; i++){
		for(var j=0; j<digits[digit][i].length; j++){
			if(digits[digit][i][j] == 1){
				scoreCtx.fillRect(sx+position*(digitWidth+digitSpace)*blockSize/2+j*blockSize/2, sy+i*blockSize/2, blockSize/2, blockSize/2);
			}
		}
	}

}

function printScore(){
	//to make it more centered 
	const fineBias = 1.5;

	var auxScore = score;
	var position = Math.floor(Math.log10(score));
	if(score == 0) position = 0;
	var bias = 0;
	if(position < 3) bias = 3-position;

	scoreCtx.clearRect(0,0,mapWidth*blockSize, 70);

	while(position >= 0){
		printDigit(auxScore%10, position + bias+fineBias);
		auxScore = Math.floor(auxScore/10);
		position--;
	}
}

function clone(o){
	return JSON.parse(JSON.stringify(o));
}

var templateBlocks = [
	[
		{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:3,y:0}	// ####
	],
	[
		{x:0,y:0},{x:1,y:0},{x:0,y:1},{x:1,y:1} // ##
												// ##
	],
	[
		{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:1,y:1} // ###
												//  #
	],
	[
		{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:2,y:1} // ###
												//   #
	],
	[
		{x:0,y:0},{x:1,y:0},{x:2,y:0},{x:0,y:1} // ###
												// #
	],
	[
		{x:0,y:1},{x:1,y:1},{x:1,y:0},{x:2,y:0} //  ##
												// ##
	],
	[
		{x:0,y:0},{x:1,y:0},{x:1,y:1},{x:2,y:1} // ##
												//  ##
	]
];

function lolLoop(){
	if(lolCounter >= 10){
		canvas.style.background = backgroundColor;
		return;
	}
	if(lolCounter % 2 == 0) canvas.style.background = lolBackgroundColor;
	else canvas.style.background = lol2BackgroundColor;
	
	lolCounter++;
	setTimeout(lolLoop, gTime/10);
}

function isLegal(){
	var legal = true;
	currentBlock.block.forEach(function (item, index){
		var x = currentBlock.x + item.x;
		var y = currentBlock.y + item.y;
		if(y >= mapHeight || x < 0 || x >= mapWidth || ( y>0 && map[y][x] != 0 ) ){
			legal = false;
		}
	});
	return legal;
}

function rotateBlock(){
	var maxY = -10;
	var minX = 10;
	currentBlock.block.forEach(function (item, index){
		if(item.y > maxY) maxY = item.y;
		if(item.x < minX) minX = item.x;
		var x = currentBlock.x + item.x;
		var a = item.x;
		item.x = item.y;
		item.y = a;
		item.x = 3-item.x;
	});
	var newMaxY = -10;
	var newMinX = 10;
	currentBlock.block.forEach(function (item, index){
		if(item.y > newMaxY) newMaxY = item.y;
		if(item.x < newMinX) newMinX = item.x;
	});
	var deltaY = maxY-newMaxY;
	var deltaX = minX - newMinX;
	
	currentBlock.block.forEach(function (item, index){
		item.y += deltaY;
		item.x += deltaX;
	});
}

function rotation(right){
	//rotateBlock wrapper
	
	if(right){
		rotateBlock();
		if(!isLegal() ){
			for(var i=0; i<3; i++) rotateBlock();
		}
	}else{
		for(var i=0; i<3; i++) rotateBlock();
		if(!isLegal() ) rotateBlock();
	}
}

function drawBlock(x, y, mode){
	if(mode){
		if(mode == MODE_NEXT_BLOCK){
			auxCtx.fillRect(blockSize*x, blockSize*y, blockSize, blockSize);
		}
		else if(mode == MODE_CURRENT_BLOCK){
			ctx.fillRect(blockSize*x, blockSize*y, blockSize, blockSize);
		}
	}
}

function drawNextBlock(){

	auxCtx.clearRect(0,0, auxCanvas.width, auxCanvas.height);
	
	//center the next block
	var minX = nextBlock.block[0].x;
	var maxX = nextBlock.block[0].x;
	nextBlock.block.forEach(function (item, index){
		if(item.x < minX) minX = item.x;
		if(item.x > maxX) maxX = item.x;
	});
	var deltaX = (auxCanvas.width - (maxX-minX+1)*blockSize)/2;


	nextBlock.block.forEach(function (item, index){

		auxCtx.fillRect((item.x)*blockSize + deltaX, (item.y+1)*blockSize, blockSize, blockSize);

	});
}

function drawMap(){
	//draw block
	currentBlock.block.forEach(function (item, index){
		var x = currentBlock.x + item.x;
		var y = currentBlock.y + item.y;
		if( x >= 0 && x < mapWidth && y >= 0 && y < mapHeight)
			map[y][x] = 1;

	});

	ctx.clearRect(0,0,blockSize*mapWidth, blockSize*mapHeight);
	for(var i=0; i<mapHeight; i++){
		for(var j=0; j<mapWidth; j++){
			if(map[i][j] == 1) drawBlock(j, i, map[i][j]);
		}
	}	


	//draw current block in other color
	currentBlock.block.forEach(function (item, index){
		ctx.fillStyle = currentBlockColor;

		ctx.fillRect((currentBlock.x+item.x)*blockSize, (currentBlock.y+item.y)*blockSize, blockSize, blockSize);

		ctx.fillStyle = blockColor;
	});

	drawNextBlock();

	printScore();
}

function newBlock(){

	return {
		block: clone(templateBlocks[Math.floor(Math.random()*blocksNo)]),
		x: mapWidth/2-2,
		y: 0
	};
}

function blockCollide(){
	//speed up!
	if(gTime > 100) gTime--;


	//draw current as normal

	currentBlock.block.forEach(function (item, index){
		ctx.fillRect((currentBlock.x+item.x)*blockSize, (currentBlock.y+item.y)*blockSize, blockSize, blockSize);
	});

	//check full line
	var doLol = false;
	var fullCounter = 0;
	for(var i=0; i<mapHeight; i++){
		var full = true;
		for(var j=0; full && j<mapWidth; j++){
			if(map[i][j] == 0) full = false;
		}	
		if( full ){
			ctx.fillStyle = lineColor;
			ctx.fillRect(0,blockSize*i,blockSize*mapWidth,blockSize);
			ctx.fillStyle = blockColor;
			for(var j=0; j<mapWidth; j++) map[0][j] = 0; //clear top line
			
			//shift down
			for(var j=i; j>0; j--){
				for(var k=0; k<mapWidth; k++){
					map[j][k] = map[j-1][k];
				}
			}
			//block one frame for animation
			block = true;
			lock = true;

			
			fullCounter++;

		}
	}

	var scoreMultiplier = 1;
	//background flash
	if(fullCounter > 0){
		lolCounter = 0;
		lolLoop();
		if(combo) scoreMultiplier = comboMultiplier;
		combo = true;
	}
	else{
		combo = false;
	}

	score += (30*fullCounter + ( (fullCounter > 1) ? (fullCounter-1)*multipleLineBonus : 0 ))*scoreMultiplier;


	currentBlock = nextBlock;
	nextBlock = newBlock();
	if( !isLegal() ){
		//game over
		endGame = true;
		ctx.fillStyle = endGameBlockColor;
		ctx.clearRect(0,0,mapWidth*blockSize, mapHeight*blockSize);
		for(var i=0; i<mapHeight; i++){
			for(var j=0; j<mapWidth; j++){
				if(map[i][j] != 0){
					ctx.fillRect(j*blockSize, i*blockSize, blockSize, blockSize);
				}
			}
		}
	}
}

function setMap(i,j,val){
	if(j >= 0 && j < mapWidth && i >= 0 && i < mapHeight)
		map[i][j] = val;
}

function mainLoop(){

	if(endGame) return;

	if(block){
		//console.log("Block PAUSE!");
		drawMap();
		lock = false;
		mainTimeout = setTimeout(mainLoop, gTime);
		block = false;
		return;
	}
	if(!currentBlock){
		currentBlock = nextBlock;
		nextBlock = newBlock();
	}else{
		currentBlock.block.forEach(function (item, index){
			setMap(currentBlock.y + item.y, currentBlock.x + item.x, 0);
			//map[currentBlock.y+item.y][currentBlock.x+item.x] = 0;
		});
		currentBlock.y += 1;
		if(!isLegal()){
			currentBlock.y -= 1;

			currentBlock.block.forEach(function (item, index){
				map[currentBlock.y+item.y][currentBlock.x+item.x] = 1;
			});

			blockCollide();
		}
	}

	if(!block) drawMap();

	mainTimeout = setTimeout(mainLoop, gTime);

}

function clearBlock(){
	
	currentBlock.block.forEach(function (item, index){
		var x = currentBlock.x + item.x;
		var y = currentBlock.y + item.y;
		if(x >= 0 && x < mapWidth && y >= 0 && y < mapHeight)
			map[y][x] = 0;
	});
}

window.onload = function () {
	currentBlock = null;
	nextBlock = newBlock();
	map = [];
	for(var i=0; i<mapHeight; i++){
		var tmpArr = [];
		for(var j=0; j<mapWidth; j++){
			tmpArr[j] = 0;
		}
		map[i] = tmpArr;
	}
	canvas = document.getElementById("canvas");
	canvas.width = mapWidth*blockSize;
	canvas.height = mapHeight*blockSize;
	canvas.style.background = backgroundColor;
	ctx = document.getElementById("canvas").getContext("2d");
	ctx.fillStyle = blockColor;

	scoreCanvas = document.getElementById("scoreCanvas");
	scoreCanvas.width = mapWidth*blockSize + auxCanvasWidth;
	scoreCanvas.style.background = scoreBackgroundColor;
	scoreCanvas.height = 70;
	// scoreCanvas.width = mapWidth*blockSize + auxCanvasWidth;
	scoreCtx = scoreCanvas.getContext("2d");
	scoreCtx.fillStyle = scoreColor;

	auxCanvas = document.getElementById("auxCanvas");
	auxCanvas.width = auxCanvasWidth;
	auxCanvas.height = canvas.height;
	auxCtx = auxCanvas.getContext("2d");
	auxCtx.fillStyle = nextBlockColor;

	playFieldContainer = document.getElementById("playFieldContainer");
	playFieldContainer.style.width = (canvas.width + auxCanvas.width) + "px";

	mainLoop();

	window.onkeypress = function (e){
		if(lock || endGame) return;

		switch(e.key){
			case "d":
				clearBlock();
				currentBlock.x += 1;
				if( !isLegal() ) currentBlock.x -= 1;
				break;
			case "a":
				clearBlock();
				currentBlock.x -= 1;
				if( !isLegal() ) currentBlock.x += 1;
				break;
			case "e":
				clearBlock();
				rotation(true);
				break;
			case "q":
				clearBlock();
				rotation(false);
				break;
			case "s":
				clearTimeout(mainTimeout);
				mainLoop();
				break;
		}
		if(e.key != "s") drawMap();
	}
}
